# shared config used when running without docker
# on mac remember to brew install apache2
LoadModule mpm_event_module lib/httpd/modules/mod_mpm_event.so
LoadModule info_module lib/httpd/modules/mod_info.so
LoadModule unixd_module lib/httpd/modules/mod_unixd.so
LoadModule authn_core_module lib/httpd/modules/mod_authn_core.so
LoadModule authz_core_module lib/httpd/modules/mod_authz_core.so
LoadModule mime_module lib/httpd/modules/mod_mime.so
LoadModule log_config_module lib/httpd/modules/mod_log_config.so
LoadModule env_module lib/httpd/modules/mod_env.so
LoadModule setenvif_module lib/httpd/modules/mod_setenvif.so
LoadModule dir_module lib/httpd/modules/mod_dir.so
LoadModule alias_module lib/httpd/modules/mod_alias.so
# pip install mod_wsgi && mod_wsgi-express module-location
LoadModule wsgi_module "/usr/local/lib/python3.11/site-packages/mod_wsgi/server/mod_wsgi-py311.cpython-311-darwin.so"

# you must use "Define serverName example.com" just before including this template
ServerName "${serverName}"


# handle redirects to specific language
RewriteEngine On

# If the user is requesting root URL and has 'lang' cookie set to 'ru'
RewriteCond %{REQUEST_URI} ^/$
RewriteCond %{HTTP:Cookie} lang=ru [NC]
RewriteRule ^$ /ru/ [L,R=302]

# If the user is requesting root URL and has 'lang' cookie set to 'en'
RewriteCond %{REQUEST_URI} ^/$
RewriteCond %{HTTP:Cookie} lang=en [NC]
RewriteRule ^$ /en/ [L,R=302]

# If no 'lang' cookie is set
# If the user is requesting root URL and has accept-language header set to 'ru'
RewriteCond %{REQUEST_URI} ^/$
RewriteCond %{HTTP:Accept-Language} ^ru [NC]
RewriteRule ^$ /ru/ [L,R=302]

# If none of the previous conditions matched
# If the user is requesting the root URL, redirect to /en/ as fallback
RewriteCond %{REQUEST_URI} ^/$
RewriteRule ^$ /en/ [L,R=302]

# apache ifdefine hack: https://serverfault.com/questions/1022233/using-ifdefine-with-environment-variables
Define "MOONSPEAK_DISABLE_AUTH_${MOONSPEAK_DISABLE_AUTH}"

<IfDefine MOONSPEAK_DISABLE_AUTH_0>
    # fake authorisation by setting wsgi variable to random UUID, just like oidc plugin does
    # TODO
</IfDefine>

LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogLevel info
ErrorLog /dev/stderr
CustomLog /dev/stdout combined

# security config see: https://httpd.apache.org/docs/2.4/en/misc/security_tips.html
# it is not recommended to throw away default apache config
LimitRequestBody 102400
ServerSignature Off
<Directory />
    Require all denied
</Directory>

# config url paths
ServerRoot "."

# enforce mod_wsgi daemon mode
WSGIDaemonProcess moonspeak threads=1
WSGIProcessGroup moonspeak
WSGIApplicationGroup %{GLOBAL}
WSGIRestrictEmbedded On

# tune event mpm see https://httpd.apache.org/docs/2.4/mod/mpm_common.html
ServerLimit 1
ThreadLimit 3
StartServers 1
MaxRequestWorkers 3
MinSpareThreads 1
MaxSpareThreads 2
ThreadsPerChild 3
# see https://httpd.apache.org/docs/2.4/mod/mpm_common.html#threadstacksize
ThreadStackSize 204800
