  "smtpServer": {
    "replyToDisplayName": "",
    "starttls": "true",
    "auth": "true",
    "envelopeFrom": "",
    "ssl": "false",
    "password": "${KEYCLOAK_SMTP_PASSWORD}",
    "port": "587",
    "host": "smtp-relay.brevo.com",
    "replyTo": "",
    "from": "moonspeak-no-reply@moonspeak.org",
    "fromDisplayName": "moonspeak",
    "user": "${KEYCLOAK_SMTP_USER}"
  },
