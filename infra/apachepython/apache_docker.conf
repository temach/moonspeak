# load inside debian docker
LoadModule mpm_event_module     /usr/lib/apache2/modules/mod_mpm_event.so
LoadModule info_module          /usr/lib/apache2/modules/mod_info.so
LoadModule authn_core_module    /usr/lib/apache2/modules/mod_authn_core.so
LoadModule authz_core_module    /usr/lib/apache2/modules/mod_authz_core.so
LoadModule mime_module          /usr/lib/apache2/modules/mod_mime.so
LoadModule dir_module           /usr/lib/apache2/modules/mod_dir.so
LoadModule alias_module         /usr/lib/apache2/modules/mod_alias.so
LoadModule rewrite_module       /usr/lib/apache2/modules/mod_rewrite.so

LoadModule wsgi_module          /usr/lib/apache2/modules/mod_wsgi.so
LoadModule auth_openidc_module  /usr/lib/apache2/modules/mod_auth_openidc.so
LoadModule authz_user_module    /usr/lib/apache2/modules/mod_authz_user.so

LoadFile /opt/opentelemetry-webserver-sdk/sdk_lib/lib/libopentelemetry_common.so
LoadFile /opt/opentelemetry-webserver-sdk/sdk_lib/lib/libopentelemetry_resources.so
LoadFile /opt/opentelemetry-webserver-sdk/sdk_lib/lib/libopentelemetry_trace.so
LoadFile /opt/opentelemetry-webserver-sdk/sdk_lib/lib/libopentelemetry_otlp_recordable.so
LoadFile /opt/opentelemetry-webserver-sdk/sdk_lib/lib/libopentelemetry_exporter_ostream_span.so
LoadFile /opt/opentelemetry-webserver-sdk/sdk_lib/lib/libopentelemetry_exporter_otlp_grpc.so
LoadFile /opt/opentelemetry-webserver-sdk/sdk_lib/lib/libopentelemetry_webserver_sdk.so
LoadModule otel_apache_module /opt/opentelemetry-webserver-sdk/WebServerModule/Apache/libmod_apache_otel.so

# you must use "Define serverName example.com" before including this template
<IfDefine !serverName>
    Error "serverName is not set. Add 'Define serverName example.com' to your apache.conf before including this config."
</IfDefine>

# you must use "Define serverName example.com" before including this template
ServerName "${serverName}"

# see: https://github.com/open-telemetry/opentelemetry-cpp-contrib/tree/main/instrumentation/otel-webserver-module#configuration
ApacheModuleEnabled On
ApacheModuleOtelSpanExporter otlp
ApacheModuleOtelExporterEndpoint "jaeger.selfhosted.moonspeak.org:4317"
ApacheModuleResolveBackends On
ApacheModuleTraceAsError Off
# you must use "Define serverName example.com" before including this template
ApacheModuleServiceName "${serverName}"
ApacheModuleServiceNamespace "${serverName}"
ApacheModuleServiceInstanceId "${serverName}"

TypesConfig /etc/mime.types
User www-data
Group www-data
Listen 80

# handle redirects to specific language
# to disable redirects, you can turn off rewrite engine in service level apache conf
RewriteEngine On

# If the user is requesting root URL and has 'lang' cookie set to 'ru'
RewriteCond %{REQUEST_URI} "^/$"
RewriteCond %{HTTP:Cookie} lang=ru [NC]
RewriteRule "^/$" /ru/ [L,R=307]

# If the user is requesting root URL and has 'lang' cookie set to 'en'
RewriteCond %{REQUEST_URI} "^/$"
RewriteCond %{HTTP:Cookie} lang=en [NC]
RewriteRule "^/$" /en/ [L,R=307]

# If no 'lang' cookie is set
# If the user is requesting root URL and has accept-language header set to 'ru'
RewriteCond %{REQUEST_URI} "^/$"
RewriteCond %{HTTP:Accept-Language} ^ru [NC]
RewriteRule "^/$" /ru/ [L,R=307]

# If none of the previous conditions matched
# If the user is requesting the root URL, redirect to /en/ as fallback
RewriteCond %{REQUEST_URI} "^/$"
RewriteRule "^/$" /en/ [L,R=307]


# apache ifdefine hack: https://serverfault.com/questions/1022233/using-ifdefine-with-environment-variables
Define "MOONSPEAK_DISABLE_AUTH_${MOONSPEAK_DISABLE_AUTH}"

<IfDefine MOONSPEAK_DISABLE_AUTH_0>
    OIDCProviderMetadataURL "${MOONSPEAK_KEYCLOAK_BACKEND_URL}/realms/moonspeak/.well-known/openid-configuration"
    OIDCClientID moonbasic
    OIDCClientSecret ${MOONSPEAK_MOONBASIC_CLIENT_SECRET}
    # used privately by mod_auth_openidc, choose a strong passphrase, see: https://github.com/OpenIDC/mod_auth_openidc/discussions/575
    OIDCCryptoPassphrase ${MOONSPEAK_SECRET_OIDC_CRYPTO_PASSPHRASE}
    # OIDCRedirectURI is a vanity URL that must point to a path protected by this module but must NOT point to any content
    # it should be registered as the Redirect or Callback URI with your client at the Provider
    OIDCRedirectURI /oauth2callback
    # requesting <redirect-uri>?info=json will show debug info about the session
    OIDCInfoHook "session"
    # maps the "sub" i.e. "subject" claim to the REMOTE_USER environment variable for wsgi
    OIDCRemoteUserClaim sub
    OIDCScope "openid email"
    OIDCSessionType "client-cookie:persistent"
    OIDCCookieDomain "moonspeak.org"
    # to test this error page, set invalid client credentials OIDCClientSecret, then it will appear every time
    OIDCHTMLErrorTemplate "../frontend/apache_oidc_html_error_template.html"
    # increase timeouts for login-state (60 * 10), session (3600 * 3) and session-refreshes (3600 * 24 * 7)
    OIDCStateTimeout 600
    OIDCSessionInactivityTimeout 10800
    OIDCSessionMaxDuration 604800
    
    <Location />
        AuthType openid-connect
        Require valid-user
        # to limit to specific user id:
        # Require claim sub:<userid>
    </Location>
</IfDefine>

LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogLevel info
ErrorLog /dev/stderr
CustomLog /dev/stdout combined

# security config see: https://httpd.apache.org/docs/2.4/en/misc/security_tips.html
# it is not recommended to throw away default apache config
LimitRequestBody 102400
ServerSignature Off
<Directory />
    Require all denied
</Directory>

# config url paths
ServerRoot "."

# enforce mod_wsgi daemon mode
# wsgi scripts will use the default "moonspeak" process-group, WSGIScriptAlias and WSGIImportScript can override that
WSGIDaemonProcess moonspeak threads=1
WSGIProcessGroup moonspeak
WSGIApplicationGroup %{GLOBAL}
WSGIRestrictEmbedded On

# tune event mpm see https://httpd.apache.org/docs/2.4/mod/mpm_common.html
ServerLimit 1
ThreadLimit 3
StartServers 1
MaxRequestWorkers 3
MinSpareThreads 1
MaxSpareThreads 2
ThreadsPerChild 3
# see https://httpd.apache.org/docs/2.4/mod/mpm_common.html#threadstacksize
ThreadStackSize 204800
